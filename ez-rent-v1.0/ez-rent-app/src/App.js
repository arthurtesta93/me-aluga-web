import React from 'react';
import './App.css';
import { hot } from 'react-hot-loader';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import NavBar from './NavBar';
import ApartmentListPage from './container/ApartmentListPage';
import ApartmentFormPage from './container/ApartmentFormPage';

function App () { 
    return(
    <>
    <Router>
    <div className="App">
        <NavBar />  
        <div className="app-container"> 
        <Switch >
        <Route path="/" component={ApartmentListPage} exact/>
        <Route path="/adicionar" component={ApartmentFormPage} />
        </Switch>    
        </div> 
    </div>
    </Router>
    <footer className="footer">
        Made with <span role="img" aria-label="heart">❤️</span> by Arthur Testa
    </footer>
    </>
    );
};

export default hot(module)(App);