import React from 'react'
import { Formik, Form } from 'formik';
import { connect } from 'react-redux';
import { createApartment } from '../redux/actions';
import * as Yup from 'yup';
import MyFormInput from './components/form/MyFormInput';
import MyCheckbox from './components/form/MyCheckbox';
import MyAddressInput from './components/form/MyAddressInput';
import StyledSubmit from './components/form/styles/StyledSubmit'

const ApartmentFormPage = ({trackedApartments, onCreatePressed}) => {
    return(
        <>
        <h2>Preencha os dados do imóvel para monitoramento:</h2>
            <Formik
                onSubmit={(values) => {
                    onCreatePressed(values); 
                }}
                initialValues={{ 
                    valorAluguel: 0,
                    valorCondominio: 0,
                    IPTU: 0,
                    link: "",
                    vaga: false,
                    cobertura: false,
                    churrasqueira: false,
                    banheirosQtd: 0
                }}
                validationSchema={Yup.object({ 
                    valorAluguel: Yup.number().max(1000000, 'Inserir valor válido').min(1, 'Inserir valor válido').required('Campo obrigatório'),
                    valorCondominio: Yup.number().max(1000000, 'Inserir valor válido').min(1, 'Inserir valor válido').required('Campo obrigatório'),
                    IPTU: Yup.number().max(1000000, 'Inserir valor válido').min(1, 'Inserir valor válido').required('Campo obrigatório'),
                    vaga: Yup.boolean().required('Campo obrigatório'),
                    cobertura: Yup.boolean().required('Campo obrigatório'),
                    churrasqueira: Yup.boolean().required('Campo obrigatório'),
                    banheirosQtd: Yup.number().max(100, 'Inserir valor válido').min(1, 'Inserir valor válido'). required('Campo obrigatório')
                })}
                /*onSubmit={(values, { setSubmitting }) => {
                    setTimeout(() => {
                      alert(JSON.stringify(values, null, 2));
                      setSubmitting(false);
                    }, 400);
                  }}*/
            >
                <Form className="form-container">
                
                <MyAddressInput 
                label="Insira o bairro ou endereço do imóvel"
                /> 
                    
                <MyFormInput 
                  label="Valor Aluguel"
                  name="valorAluguel"
                  type="number"
                  placeholder="0"
                />
                <MyFormInput 
                  label="Valor Condomínio"
                  name="valorCondominio"
                  type="number"
                  placeholder="0"
                />
                <MyFormInput 
                  label="Valor IPTU"
                  name="IPTU"
                  type="number"
                  placeholder="0"
                />
                <MyFormInput 
                  label="Link de acesso à pagina"
                  name="link"
                  type="url"
                  placeholder="Copie e cole a URL da página da imobiliária"
                />
                <MyCheckbox  
                  name="vaga"
                  children="Imóvel possui vaga de estacionamento?"
                />                
                <MyCheckbox  
                  name="cobertura"
                  children="Imóvel possui cobertura?"
                />
                <MyCheckbox  
                  name="churrasqueira"
                  children="Imóvel possui churrasqueira?"
                /> 
                <MyFormInput 
                  label="Banheiros (quantidade)"
                  name="banheirosQtd"
                  type="number"
                  placeholder="0"
                />
                <StyledSubmit type="submit">Enviar</StyledSubmit>
                </Form>
            </Formik>
        </>
    )
}

const mapStateToProps = state => ({
  trackedApartments: state.trackedApartments
});

const mapDispatchToProps = dispatch => ({
  onCreatePressed: values => dispatch(createApartment(values)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ApartmentFormPage);