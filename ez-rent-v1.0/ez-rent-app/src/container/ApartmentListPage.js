import React from 'react';
import ApartmentListComponent from './components/ApartmentListComponent';
import apartmentData from '../../../ez-rent-server/apartment-data.json';

const ApartmentListPage = () => {
    return ( 
        <ApartmentListComponent apartamentosLista={apartmentData} />  
    )
}

export default ApartmentListPage;