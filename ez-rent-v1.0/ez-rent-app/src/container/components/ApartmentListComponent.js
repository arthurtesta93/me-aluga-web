import React from 'react'
import StyledApartmentDisplay from './styles/StyledApartmentDisplay';

const ApartmentListComponent = ({apartamentosLista}) => {

    const apartmentArr = apartamentosLista.apartamentos;
    //TODO onMouseOver fade in modal with apartment details 
    return(
        <>
        <h3>Apartamentos salvos:</h3>
        
        {apartmentArr.map((apartamento, key) => (
            <StyledApartmentDisplay key={key} onClick={() => window.open(apartamento.link)}>
                ID: {apartamento.id} <br />
                Valor total (incl. IPTU): R$ {apartamento.valorAluguel + apartamento.valorCondominio + apartamento.IPTU} <br/>
                Bairro: {apartamento.bairro} <br/>  
            </StyledApartmentDisplay>           
        ))}
        </>
    )
}

export default ApartmentListComponent;