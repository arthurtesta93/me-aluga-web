import React, { useState } from 'react';   
import PlacesAutoComplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete'; 
import StyledLabel from './styles/StyledLabel';
import StyledInput from './styles/StyledInput';

const MyAddressInput = ({label, ...props}) => {
   
    const [address, setAddress] = useState(""); 

    const handleSelect = () => {
        setTimeout(() => {
          alert(JSON.stringify(address, null, 2));
        }, 400);
      }

    const searchOptions = {
        location: new google.maps.LatLng(-30, -51),
        radius: 2000,
        types: ['(regions)'], 
      } 
      
    return (
        <>
        <StyledLabel htmlFor={props.id || props.name}>{label}</StyledLabel>

        <PlacesAutoComplete
            value={address}
            onChange={setAddress}
            onSelect={handleSelect}
            searchOptions={searchOptions}
        >{({getInputProps, suggestions, getSuggestionItemProps, loading }) => (
            <div>
                <StyledInput className="text-input" {...props} {...getInputProps({placeholder:"Rio Branco, Porto Alegre - RS"})}/>
                <div>
                    {loading ? <div>Aguarde...</div> : null}

                    {suggestions.map((suggestion, key) => {
                        const style = {
                            backgroundColor: suggestion.active ? "#ACC18A" : "#fff"
                        };
                        return (
                            <div key={key}  {...getSuggestionItemProps(suggestion, { style })}>
                                {suggestion.description}
                            </div>
                        );    
                    })}
                </div>
            </div>
        )}
        </PlacesAutoComplete>
        </>
    );
};

export default MyAddressInput;
