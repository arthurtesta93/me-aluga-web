import React from 'react';
import { useField } from 'formik';
import StyledLabel from './styles/StyledLabel';
import StyledErrorMessage from './styles/StyledErrorMessage';

const MyCheckbox = ({ children, ...props }) => {
 
    const [field, meta] = useField({ ...props, type: 'checkbox' });

    return (
      <>
        <StyledLabel className="checkbox">
        {children}
          <input type="checkbox" {...field} {...props} />
        </StyledLabel>
        {meta.touched && meta.error ? (
          <StyledErrorMessage>{meta.error}</StyledErrorMessage>
        ) : null}
        <br />
      </>
    );
  };
  
  export default MyCheckbox;