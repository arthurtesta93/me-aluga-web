import React from 'react';
import { useField } from 'formik';
import StyledInput from './styles/StyledInput';
import StyledLabel from './styles/StyledLabel';
import StyledErrorMessage from './styles/StyledErrorMessage'

const MyTextInput = ({ label, ...props }) => {

    const [field, meta] = useField(props);

    return (
        <>
            <StyledLabel htmlFor={props.id || props.name}>{label}</StyledLabel>
            <StyledInput className="text-input" {...field} {...props} />
            {meta.touched && meta.error ? (
                <StyledErrorMessage className="error">{meta.error}</StyledErrorMessage>
            ) : null}
            <br />
        </>
    );
};

export default MyTextInput;
