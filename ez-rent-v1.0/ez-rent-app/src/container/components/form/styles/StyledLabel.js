import styled from 'styled-components'; 

const StyledLabel = styled.label`
    margin-top: 1rem;   
    color: black;  
    padding: 0.25em 1em;
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
    :hover{
        color: dark-gray;
        font-weight: bold;
        opacity: 60%;
        transition: opacity 1s;
    }
`;




export default StyledLabel;
