import styled from 'styled-components';

    
const StyledSubmit = styled.button`
    font-size: 1em; 
    cursor: pointer;
    margin: 1em;
    padding: 0.25em 1em;
    border: 2px solid gray; 
    border-radius: 3px; 
`

export default StyledSubmit;