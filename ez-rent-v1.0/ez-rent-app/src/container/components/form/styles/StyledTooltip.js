import styled from 'styled-components';
import StyledLabel from './StyledLabel';

const StyledTooltip = styled.span`
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    bottom: 100%;
    left: 50%;
    margin-left: -60px;
    opacity: 0;
    transition: opacity 1s;
    ${StyledLabel}:hover & {
        visibility: visible;
        opacity: 1;
    }


`;

export default StyledTooltip;