import styled from 'styled-components';

const StyledApartmentDisplay = styled.button`
    background: transparent;
    border-radius: 3px;
    border: 2px solid #F2AF29;
    color: #AD343E;
        &:hover{
            color: white;
            background: #F2AF29;
        }   
    margin: 0 1em;
    padding: 0.25em 1em;
    cursor: pointer;
    transition: background 1s;
`

export default StyledApartmentDisplay; 