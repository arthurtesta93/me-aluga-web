export const CREATE_APARTMENT = 'CREATE_APARTMENT';

export const createApartment = apartmentInfo => ({
    type: CREATE_APARTMENT,
    payload: { apartmentInfo },
});

