import { CREATE_APARTMENT } from './actions';
 

export const trackedApartments = ( state = [], action ) => {
     
    const {type, payload} = action;
    
    switch (type) {
        case CREATE_APARTMENT: {
            const { apartmentInfo } = payload;
            const newApartment = {
                apartmentInfo,
            };
            return state.concat(newApartment);
        } 
    }

    return state;
};