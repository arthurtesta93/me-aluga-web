import { createStore, combineReducers } from 'redux';
import { trackedApartments } from './reducers';
const reducers = {
    trackedApartments
};

const rootReducer = combineReducers(reducers);

export const configureStore = () => 
    createStore(
        rootReducer,
        window.__REDUX_DEVTOOLS_EXTENSION__ &&
        window.__REDUX_DEVTOOLS_EXTENSION__ (),
);